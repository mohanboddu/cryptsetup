Summary: Utility for setting up encrypted disks
Name: cryptsetup
Version: 2.3.6
Release: 1%{?dist}
License: GPLv2+ and LGPLv2+
URL: https://gitlab.com/cryptsetup/cryptsetup
BuildRequires: openssl-devel, popt-devel, device-mapper-devel
BuildRequires: libuuid-devel, gcc, json-c-devel
BuildRequires: libpwquality-devel, libblkid-devel
BuildRequires: make
Provides: cryptsetup-luks = %{version}-%{release}
Obsoletes: cryptsetup-luks < 1.4.0
Requires: cryptsetup-libs = %{version}-%{release}
Requires: libpwquality >= 1.2.0

%global upstream_version %{version}
Source0: https://www.kernel.org/pub/linux/utils/cryptsetup/v2.3/cryptsetup-%{upstream_version}.tar.xz
# Following patch has to applied last
Patch9999: %{name}-add-system-library-paths.patch

%description
The cryptsetup package contains a utility for setting up
disk encryption using dm-crypt kernel module.

%package devel
Requires: %{name}-libs%{?_isa} = %{version}-%{release}
Requires: pkgconfig
Summary: Headers and libraries for using encrypted file systems
Provides: cryptsetup-luks-devel = %{version}-%{release}
Obsoletes: cryptsetup-luks-devel < 1.4.0

%description devel
The cryptsetup-devel package contains libraries and header files
used for writing code that makes use of disk encryption.

%package libs
Summary: Cryptsetup shared library
Provides: cryptsetup-luks-libs = %{version}-%{release}
Obsoletes: cryptsetup-luks-libs < 1.4.0

%description libs
This package contains the cryptsetup shared library, libcryptsetup.

%package -n veritysetup
Summary: A utility for setting up dm-verity volumes
Requires: cryptsetup-libs = %{version}-%{release}

%description -n veritysetup
The veritysetup package contains a utility for setting up
disk verification using dm-verity kernel module.

%package -n integritysetup
Summary: A utility for setting up dm-integrity volumes
Requires: cryptsetup-libs = %{version}-%{release}

%description -n integritysetup
The integritysetup package contains a utility for setting up
disk integrity protection using dm-integrity kernel module.

%package reencrypt
Summary: A utility for offline reencryption of LUKS encrypted disks
Requires: cryptsetup-libs = %{version}-%{release}

%description reencrypt
This package contains cryptsetup-reencrypt utility which
can be used for offline reencryption of disk in situ.

%prep
%autosetup -n cryptsetup-%{upstream_version} -p 1
chmod -x misc/dracut_90reencrypt/*

%build
%configure --enable-fips --enable-pwquality --enable-internal-sse-argon2
%make_build

%install
%make_install
rm -rf %{buildroot}%{_libdir}/*.la

%find_lang cryptsetup

%ldconfig_scriptlets -n cryptsetup-libs

%files
%license COPYING
%doc AUTHORS FAQ docs/*ReleaseNotes
%{_mandir}/man8/cryptsetup.8.gz
%{_sbindir}/cryptsetup

%files -n veritysetup
%license COPYING
%{_mandir}/man8/veritysetup.8.gz
%{_sbindir}/veritysetup

%files -n integritysetup
%license COPYING
%{_mandir}/man8/integritysetup.8.gz
%{_sbindir}/integritysetup

%files reencrypt
%license COPYING
%doc misc/dracut_90reencrypt
%{_mandir}/man8/cryptsetup-reencrypt.8.gz
%{_sbindir}/cryptsetup-reencrypt

%files devel
%doc docs/examples/*
%{_includedir}/libcryptsetup.h
%{_libdir}/libcryptsetup.so
%{_libdir}/pkgconfig/libcryptsetup.pc

%files libs -f cryptsetup.lang
%license COPYING COPYING.LGPL
%{_libdir}/libcryptsetup.so.*
%{_tmpfilesdir}/cryptsetup.conf
%ghost %attr(700, -, -) %dir /run/cryptsetup

%changelog
* Wed Jun 16 2021 Ondrej Kozina <okozina@redhat.com> - 2.3.6-1
- Update to cryptsetup 2.3.6.
- Resolves: #1961291 #1970932

* Tue Jun 15 2021 Mohan Boddu <mboddu@redhat.com> - 2.3.5-5
- Rebuilt for RHEL 9 BETA for openssl 3.0

Related: rhbz#1971065

* Tue Apr 27 2021 Ondrej Kozina <okozina@redhat.com> - 2.3.5-4
- Drop dependency on libargon2
- Resolves: #1936959

* Thu Apr 15 2021 Mohan Boddu <mboddu@redhat.com> - 2.3.5-3
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Thu Mar 11 2021 Milan Broz <gmazyland@gmail.com> - 2.3.5-1
- Update to cryptsetup 2.3.5.

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 2.3.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Thu Sep 03 2020 Milan Broz <gmazyland@gmail.com> - 2.3.4-1
- Update to cryptsetup 2.3.4.
- Fix for CVE-2020-14382 (#1874712)
